package controllers

import play.api._
import play.api.mvc._

import javax.mail._
import javax.mail.internet._
import javax.mail.search._
import java.util.Properties

import play.api.libs.iteratee._
import play.api.libs.concurrent.Execution.Implicits._
import play.api.libs.json._
import scala.util.matching.Regex
import scala.collection.mutable

import java.io._


object Application extends Controller {

  def index = Action {
    Ok(views.html.index(""))
  }

  def processMail = WebSocket.using[String] { request => 

    val props = System.getProperties()
    props.setProperty("mail.store.protocol", "imaps")
    val session = javax.mail.Session.getDefaultInstance(props, null)
    val store = session.getStore("imaps")
  
    val (out,channel) = Concurrent.broadcast[String]

    //log the message to stdout and send response back to client
    val in = Iteratee.foreach[String] {
      msg => {
        
        val credentials: JsValue = Json.parse(msg)

        val email = (credentials \ "email" ).asOpt[String]
        val password = (credentials \ "password" ).asOpt[String]

        if(email.isEmpty && password.isEmpty) {
          channel push("{'error':'01'}")

        } else {

          try {
            store.connect("mail.phantasia.pe", email.get , password.get )
            val inbox = store.getFolder("Inbox")
            inbox.open(Folder.READ_ONLY)
            
            val messages = inbox.getMessages()
            val totalMessages = messages.size
            val firstMessageDate = messages.head.getReceivedDate()
            val lastMessageDate = messages.last.getReceivedDate()

            println("Analizando mensajes desde : "+firstMessageDate)
            println("Analizando mensajes hasta : "+lastMessageDate)
            println("Cantidad de correos : "+totalMessages)

            val coincidences:mutable.Buffer[List[String]] = mutable.Buffer.empty
            def addCoincidences(
              subject:String, 
              body:String, 
              from:String,
              date:String
              ) = {
              coincidences += List(subject, body)
            }


            for ((message,i) <- messages.view.zipWithIndex ) {
              val percent = (i*100) / messages.size

              //println("subject: "+message.getSubject())
              //println("body: "+message.getContent() )

              val messageContent = message.getSubject()
              val subject = if(messageContent != null) 
                              messageContent.toLowerCase 
                            else ""
              val from = InternetAddress.toString(message.getFrom())
              val recievedDate = message.getReceivedDate().toString()

              val body:Option[String] = message.getContent() match {
                case s:String => Some(s)
                case m:Multipart => {
                  println("multipart")
                  val message = for (i <- 0 until m.getCount()) yield {
                    try{
                      m.getBodyPart(i).getContent() match {
                        case t:String => t
                        case m:BodyPart => None
                        case _ => None
                      }
                    } catch {
                      case io:IOException => println("ioerror " +io.getMessage())
                      case me:MessagingException => println("messageerror")
                    }
                  }
                  Some(message.toList.mkString(" "))
                }
                case _ => None
              }


              val pattern = new Regex("urgente")

              val foundSubject = !(pattern findAllIn subject).isEmpty
              val foundBody = !(pattern findAllIn body.getOrElse("")).isEmpty

              if(foundSubject || foundBody) 
                addCoincidences(subject, body.getOrElse(""), from, recievedDate)

              println(coincidences.size+" de "+i)

              val data = Json.toJson(
                Map(  
                  "percent"->percent, 
                  "index"->i,
                  "coincidences"->coincidences.size 
                  )
                )

              channel push(Json.stringify(data))
            }

            inbox.close(true)
            
          } catch {
            case e: NoSuchProviderException =>  e.printStackTrace()
                                                System.exit(1)
            case me: MessagingException =>      me.printStackTrace()
                                                System.exit(2)
            case fe:FolderClosedException =>    fe.printStackTrace()
                                                System.exit(2)
          } finally {
            store.close()
          }  
        }        
       }
    }
    (in, out)
  }

}