name := "mail_analyzer_v2"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  "javax.mail" % "mail" % "1.4.1"
)     

play.Project.playScalaSettings
